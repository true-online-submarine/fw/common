#include "main.h"
#include "sub_errors.h"
#include "sub_logger.h"

void submarine_assert_func(int assert_num, int is_critical)
{
    int iter = 0;

    logger_dgb_print("[ERR]: %s ASSERT_%d\n",
                     (is_critical == SUB_ASSERT_CRITICAL) ? "critical" : "non critical",
                     assert_num);

    if (is_critical) {
        while (1) {
            ++iter;

            HAL_GPIO_TogglePin(ONBOARD_LED_GPIO_Port, ONBOARD_LED_Pin);
            HAL_Delay(100);

            if (iter == 10) {
                HAL_NVIC_SystemReset();
            }
        }
    }
}
