#ifndef __COMMON_SUB_LOGGER_H
#define __COMMON_SUB_LOGGER_H

#ifdef __cplusplus
extern "C" {
#endif

void logger_dgb_print(const char *format, ...);

#ifdef __cplusplus
}
#endif

#endif /* __COMMON_SUB_LOGGER_H */
